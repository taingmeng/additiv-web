import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Alert, Container } from 'reactstrap';

import { clearMessage } from '../redux/app/actions';
import { getMessage, getMessageLevel } from '../redux/app/selectors';
import { MessageLevel } from '../redux/app/types';
import { RootState } from '../redux/store';

interface AppAlertProps {
  message: string;
  messageLevel: MessageLevel | null;
  clearMessage: () => void;
}

const AppAlert: React.FC<AppAlertProps> = ({
  message,
  messageLevel,
  clearMessage,
}) => {
  useEffect(() => {
    let id = null;
    if (message) {
      if (id) {
        clearTimeout(id);
      }
      id = setTimeout(clearMessage, 5000);
    }
    return () => {
      clearTimeout(id);
    };
  }, [message, clearMessage]);
  return (
    <Container className="fixed-bottom">
      <Alert isOpen={!!message} toggle={clearMessage} color={messageLevel}>
        <span className="alert-inner--icon">
          <i className="ni ni-support-16" />
        </span>
        <span className="alert-inner--text ml-1">{message}</span>
      </Alert>
    </Container>
  );
};

const mapStateToProps = (state: RootState) => ({
  message: getMessage(state),
  messageLevel: getMessageLevel(state),
});

const mapDispatchToProps = {
  clearMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppAlert);
