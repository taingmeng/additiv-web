import React from 'react';
import { Container, Row, Col } from 'reactstrap';

const DefaultFooter: React.FC = () => {
  return (
    <>
      <footer className="footer fixed-bottom ">
        <Container>
          <Row className=" align-items-center justify-content-md-between">
            <Col md="6">
              <div className="copyright">
                © {new Date().getFullYear()}{' '}
                <a
                  href="https://www.linkedin.com/in/meng-taing-89490850/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Meng Taing
                </a>
              </div>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
};

export default DefaultFooter;
