import React from 'react';
import {
  UncontrolledCollapse,
  NavbarBrand,
  Navbar,
  Container,
  Row,
  Col,
} from 'reactstrap';

const DefaultNavbar: React.FC = () => {
  return (
    <Navbar className="navbar-dark bg-primary" expand="lg">
      <Container>
        <NavbarBrand href="/">Additiv</NavbarBrand>
        <button className="navbar-toggler" id="navbar-primary">
          <span className="navbar-toggler-icon" />
        </button>
        <UncontrolledCollapse navbar toggler="#navbar-primary">
          <div className="navbar-collapse-header">
            <Row>
              <Col className="collapse-brand" xs="6">
                <NavbarBrand
                  className="text-dark"
                  href="/"
                  onClick={(e) => e.preventDefault()}
                >
                  Additiv
                </NavbarBrand>
              </Col>
              <Col className="collapse-close" xs="6">
                <button className="navbar-toggler" id="navbar-primary">
                  <span />
                  <span />
                </button>
              </Col>
            </Row>
          </div>
        </UncontrolledCollapse>
      </Container>
    </Navbar>
  );
};
export default DefaultNavbar;
