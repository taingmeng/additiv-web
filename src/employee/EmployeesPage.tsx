import React, { useEffect, useState } from 'react';
import { FormGroup, Container, Row, Col, Button } from 'reactstrap';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { Typeahead } from 'react-bootstrap-typeahead';

import DefaultNavbar from '../components/DefaultNavbar';
import { fetchEmployees } from '../redux/employee/actions';
import { RootState } from '../redux/store';
import { getEmployees, getLoading } from '../redux/employee/selectors';
import { useHistory } from 'react-router-dom';
import { EmployeesResponse } from '../redux/employee/types';
import DefaultFooter from '../components/DefaultFooter';

interface EmployeesPageProps {
  employees: EmployeesResponse;
  fetchEmployees: () => void;
}

const EmployeesPage: React.FC<EmployeesPageProps> = ({
  employees,
  fetchEmployees,
}) => {
  const [searchAltFocused, setSearchAltFocused] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [singleSelections, setSingleSelections] = useState([]);
  const [hasEnter, setHasEnter] = useState(false);

  const history = useHistory();

  useEffect(() => {
    fetchEmployees();
  }, [fetchEmployees]);

  useEffect(() => {
    if (hasEnter) {
      setHasEnter(false);
      if (singleSelections[0] || searchQuery.trim()) {
        history.push(
          `/overview?name=${singleSelections[0] || searchQuery.trim()}`,
        );
      }
    }
  }, [hasEnter, singleSelections, searchQuery, history]);

  return (
    <>
      <DefaultNavbar />
      <Container className="section-hero pt-7">
        <h3 className="h4 text-success font-weight-bold mb-4">
          Search employees
        </h3>
        <Row className="justify-content-center">
          <Col sm="10" xs="12">
            <FormGroup
              className={classnames({
                focused: searchAltFocused,
              })}
            >
              <Typeahead
                id="basic-typeahead-single"
                labelKey="name"
                onChange={setSingleSelections}
                onInputChange={setSearchQuery}
                options={employees}
                onFocus={() => setSearchAltFocused(true)}
                onBlur={() => setSearchAltFocused(false)}
                placeholder="Search an employee..."
                selected={singleSelections}
                onKeyDown={(e) => {
                  //@ts-ignore
                  if (e.key === 'Enter') {
                    setTimeout(() => {
                      setHasEnter(true);
                    }, 100);
                  }
                }}
              />
            </FormGroup>
          </Col>
          <Col sm="2" xs="12" className="mt-3 mt-sm-0">
            <Button
              color="primary"
              type="button"
              onClick={() => {
                setTimeout(() => {
                  setHasEnter(true);
                }, 100);
              }}
            >
              Search
            </Button>
          </Col>
        </Row>
      </Container>
      <DefaultFooter />
    </>
  );
};

const mapStateToProps = (state: RootState) => ({
  loading: getLoading(state),
  employees: getEmployees(state),
});

const mapDispatchToProps = {
  fetchEmployees,
};

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(EmployeesPage);
