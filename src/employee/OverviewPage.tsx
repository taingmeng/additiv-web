import React, { useEffect } from 'react';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';

import DefaultNavbar from '../components/DefaultNavbar';
import { fetchEmployeeDetails } from '../redux/employee/actions';
import { RootState } from '../redux/store';
import { getLoading, getCollection } from '../redux/employee/selectors';
import { useQuery } from '../hooks/useQuery';
import { EmployeeCollection } from '../redux/employee/types';
import SubordinateCard from './SubordinateCard';
import DefaultFooter from '../components/DefaultFooter';

interface OverviewPageProps {
  collection: EmployeeCollection;
  fetchEmployeeDetails: (name: string) => void;
}

const OverviewPage: React.FC<OverviewPageProps> = ({
  fetchEmployeeDetails,
  collection,
}) => {
  const query = useQuery();
  const employeeName = query.get('name');
  const details = collection[employeeName];

  useEffect(() => {
    fetchEmployeeDetails(employeeName);
  }, [fetchEmployeeDetails, employeeName]);

  return (
    <>
      <DefaultNavbar />
      <Container className="mt-3 mb-7">
        <h3 className="display-3">{employeeName}</h3>
        {!details && (
          <small className="text-uppercase text-muted font-weight-bold">
            No employee found with this name.
          </small>
        )}
        {!!details && (
          <>
            <h3 className="h4 text-success font-weight-bold mb-4">
              {details.position}
            </h3>

            {!!details.directSubordinates.length && (
              <>
                <small className="d-block text-uppercase font-weight-bold mb-3">
                  Direct Subordinates
                </small>
                {details.directSubordinates.map((sub) => (
                  <SubordinateCard
                    key={sub}
                    name={sub}
                    details={collection[sub]}
                  />
                ))}
              </>
            )}

            {!!details.indirectSubordinates.length && (
              <>
                <small className="d-block text-uppercase font-weight-bold mb-3 mt-5">
                  Indirect Subordinates
                </small>
                {details.indirectSubordinates.map((sub) => (
                  <SubordinateCard
                    key={sub}
                    name={sub}
                    details={collection[sub]}
                  />
                ))}
              </>
            )}
          </>
        )}
      </Container>
      <DefaultFooter />
    </>
  );
};

const mapStateToProps = (state: RootState) => ({
  loading: getLoading(state),
  collection: getCollection(state),
});

const mapDispatchToProps = {
  fetchEmployeeDetails,
};

//@ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(OverviewPage);
