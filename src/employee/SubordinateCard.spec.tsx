import React from 'react';
import { render, screen } from '@testing-library/react';
import SubordinateCard from './SubordinateCard';

describe('SubordinateCard', () => {
  it('should render name', () => {
    render(<SubordinateCard name="Test Name" />);

    const name = screen.getByTestId('SubordinateCard_Name');
    expect(name).toHaveTextContent('Test Name');

    const position = screen.queryByTestId('SubordinateCard_Position');
    expect(position).toBeNull();
  });

  it('should render position if details is provided', () => {
    render(
      <SubordinateCard
        name="Test Name"
        details={{
          name: 'Test Name',
          position: 'Test Position',
          directSubordinates: [],
          indirectSubordinates: [],
        }}
      />,
    );

    const name = screen.getByTestId('SubordinateCard_Name');
    expect(name).toHaveTextContent('Test Name');

    const position = screen.getByTestId('SubordinateCard_Position');
    expect(position).toHaveTextContent('Test Position');
  });
});
