import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { Employee } from '../redux/employee/types';

interface SubordinateCardProps {
  name: string;
  details?: Employee;
}

const SubordinateCard: React.FC<SubordinateCardProps> = ({ name, details }) => {
  const history = useHistory();

  return (
    <Card className="shadow mt-3" data-testid="SubordinateCard">
      <CardBody
        role="button"
        onClick={() => history.push(`/overview?name=${name}`)}
      >
        <Row className="align-items-center">
          <Col xs="10">
            <strong className="d-block" data-testid="SubordinateCard_Name">
              {name}
            </strong>
            {!!details && (
              <small className="text" data-testid="SubordinateCard_Position">
                {details.position}
              </small>
            )}
          </Col>
          <Col xs="2" className="text-right">
            <i className="fa fa-chevron-right"></i>
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default SubordinateCard;
