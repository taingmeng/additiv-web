import { useQuery } from './useQuery';

describe('useQuery', () => {
  it('should ', () => {
    const query = useQuery();

    expect(query.get('name')).toEqual('John Doe');
    expect(query.get('unknown')).toBeNull();
  });
});
