import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';

import reportWebVitals from './reportWebVitals';
import EmployeesPage from './employee/EmployeesPage';
import OverviewPage from './employee/OverviewPage';
import AppAlert from './components/AppAlert';

import './assets/vendor/nucleo/css/nucleo.css';
import './assets/vendor/font-awesome/css/font-awesome.min.css';
import './assets/scss/argon-design-system-react.scss?v1.1.0';

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route
          path="/"
          exact
          render={(props) => <EmployeesPage {...props} />}
        />
        <Route
          path="/overview"
          exact
          render={(props) => <OverviewPage {...props} />}
        />
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
    <AppAlert />
  </Provider>,
  document.getElementById('root'),
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
