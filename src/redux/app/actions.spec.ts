import { createRootStore } from '../store';
import { clearMessage, setErrorMessage } from './actions';

describe('app/actions', () => {
  it('should set error message', () => {
    const store = createRootStore();
    store.dispatch(setErrorMessage('Test error message'));

    expect(store.getState().app.message).toEqual('Test error message');
    expect(store.getState().app.messageLevel).toEqual('danger');
  });

  it('should clear message', () => {
    const store = createRootStore();
    store.dispatch(setErrorMessage('Test error message'));
    store.dispatch(clearMessage());

    expect(store.getState().app.message).toEqual('');
    expect(store.getState().app.messageLevel).toBeNull();
  });
});
