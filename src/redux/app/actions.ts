import { SetErrorMessage, ClearMessage } from './types';

export const CLEAR_MESSAGE = 'Additiv/app/CLEAR_MESSAGE';
export const SET_ERROR_MESSAGE = 'Additiv/app/SET_ERROR_MESSAGE';

export const setErrorMessage = (message: string): SetErrorMessage => ({
  type: SET_ERROR_MESSAGE,
  payload: message,
});

export const clearMessage = (): ClearMessage => ({
  type: CLEAR_MESSAGE,
});
