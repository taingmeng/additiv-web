import { CLEAR_MESSAGE, SET_ERROR_MESSAGE } from './actions';
import { AppAction, MessageLevel } from './types';

interface AppState {
  message: string;
  messageLevel: MessageLevel | null;
}

export const initialState: AppState = {
  message: '',
  messageLevel: null,
};

const EmployeeReducer = (
  state: AppState = initialState,
  action: AppAction,
): AppState => {
  switch (action.type) {
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        message: action.payload,
        messageLevel: 'danger',
      };
    case CLEAR_MESSAGE:
      return {
        ...state,
        message: '',
        messageLevel: null,
      };
    default:
      return state;
  }
};

export default EmployeeReducer;
