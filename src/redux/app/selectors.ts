import { RootState } from '../store';
import { MessageLevel } from './types';

export const getMessage = (state: RootState): string => state.app.message;
export const getMessageLevel = (state: RootState): MessageLevel | null =>
  state.app.messageLevel;
