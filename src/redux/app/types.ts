import { CLEAR_MESSAGE, SET_ERROR_MESSAGE } from './actions';

export interface SetErrorMessage {
  type: typeof SET_ERROR_MESSAGE;
  payload: string;
}

export interface ClearMessage {
  type: typeof CLEAR_MESSAGE;
}

export type AppAction = ClearMessage | SetErrorMessage;

export type MessageLevel = 'danger' | 'info';
