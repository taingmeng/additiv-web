import { waitFor } from '@testing-library/react';
import axios from 'axios';
import { createRootStore } from '../store';
import { fetchEmployeeDetails, fetchEmployees } from './actions';

describe('employee/actions', () => {
  describe('fetchEmployees', () => {
    it('should store fetched employees when success', async () => {
      jest.spyOn(axios, 'get').mockResolvedValueOnce({
        data: ['John Doe', 'Jane Doe'],
      });
      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployees());

      await waitFor(() =>
        expect(store.getState().employee.employees).toEqual([
          'John Doe',
          'Jane Doe',
        ]),
      );
    });

    it('should set error message when error', async () => {
      jest.spyOn(axios, 'get').mockRejectedValueOnce(new Error('Test error'));
      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployees());

      await waitFor(() =>
        expect(store.getState().app.message).toEqual('Test error'),
      );
      expect(store.getState().employee.employees).toEqual([]);
    });
  });

  describe('fetchEmployeeDetails', () => {
    it('should call fetch employee details once if no subordinate', async () => {
      jest.spyOn(axios, 'get').mockResolvedValueOnce({
        data: ['employee'],
      });
      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployeeDetails('John Doe'));

      await waitFor(() =>
        expect(store.getState().employee.collection).toEqual({
          'John Doe': {
            directSubordinates: [],
            indirectSubordinates: [],
            name: 'John Doe',
            position: 'employee',
          },
        }),
      );

      expect(axios.get).toHaveBeenCalledTimes(1);
      expect(axios.get).toHaveBeenCalledWith(
        'https://api.additivasia.io/api/v1/assignment/employees/John%20Doe',
      );
    });

    it('should skip one subordinate fetch if the subordinate has been fetched', async () => {
      jest
        .spyOn(axios, 'get')
        .mockResolvedValueOnce({
          data: ['CEO', { 'direct-subordinates': ['James Doe'] }],
        })
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['Jack Doe'] }],
        })
        .mockResolvedValueOnce({
          data: ['employee'],
        })
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['James Doe'] }],
        });

      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployeeDetails('John Doe'));

      await waitFor(() =>
        expect(store.getState().employee.collection).toEqual({
          'Jack Doe': {
            directSubordinates: [],
            indirectSubordinates: [],
            name: 'Jack Doe',
            position: 'employee',
          },
          'James Doe': {
            directSubordinates: ['Jack Doe'],
            indirectSubordinates: [],
            name: 'James Doe',
            position: 'manager',
          },
          'John Doe': {
            directSubordinates: ['James Doe'],
            indirectSubordinates: ['Jack Doe'],
            name: 'John Doe',
            position: 'CEO',
          },
        }),
      );

      //@ts-ignore
      store.dispatch(fetchEmployeeDetails('Jane Doe'));

      await waitFor(() =>
        expect(store.getState().employee.collection).toEqual({
          'Jack Doe': {
            directSubordinates: [],
            indirectSubordinates: [],
            name: 'Jack Doe',
            position: 'employee',
          },
          'James Doe': {
            directSubordinates: ['Jack Doe'],
            indirectSubordinates: [],
            name: 'James Doe',
            position: 'manager',
          },
          'Jane Doe': {
            directSubordinates: ['James Doe'],
            indirectSubordinates: ['Jack Doe'],
            name: 'Jane Doe',
            position: 'manager',
          },
          'John Doe': {
            directSubordinates: ['James Doe'],
            indirectSubordinates: ['Jack Doe'],
            name: 'John Doe',
            position: 'CEO',
          },
        }),
      );

      expect(axios.get).toHaveBeenCalledTimes(4);
      //@ts-ignore
      expect(axios.get.mock.calls).toEqual([
        ['https://api.additivasia.io/api/v1/assignment/employees/John%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/James%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/Jack%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/Jane%20Doe'],
      ]);
    });

    it('should handle cycle hierarchy John -> Jane -> James -> John', async () => {
      jest
        .spyOn(axios, 'get')
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['Jane Doe'] }],
        })
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['James Doe'] }],
        })
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['John Doe'] }],
        })
        .mockResolvedValueOnce({
          data: ['manager', { 'direct-subordinates': ['Jane Doe'] }],
        });

      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployeeDetails('John Doe'));

      await waitFor(() =>
        expect(store.getState().employee.collection).toEqual({
          'James Doe': {
            directSubordinates: ['John Doe'],
            indirectSubordinates: [],
            name: 'James Doe',
            position: 'manager',
          },
          'Jane Doe': {
            directSubordinates: ['James Doe'],
            indirectSubordinates: [],
            name: 'Jane Doe',
            position: 'manager',
          },
          'John Doe': {
            directSubordinates: ['Jane Doe'],
            indirectSubordinates: ['James Doe'],
            name: 'John Doe',
            position: 'manager',
          },
        }),
      );
      expect(axios.get).toHaveBeenCalledTimes(4);
      //@ts-ignore
      expect(axios.get.mock.calls).toEqual([
        ['https://api.additivasia.io/api/v1/assignment/employees/John%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/Jane%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/James%20Doe'],
        ['https://api.additivasia.io/api/v1/assignment/employees/John%20Doe'],
      ]);
    });

    it('should set error message when error', async () => {
      jest
        .spyOn(axios, 'get')
        .mockResolvedValueOnce({
          data: ['CEO', { 'direct-subordinates': ['Jane Doe', 'James Doe'] }],
        })
        .mockRejectedValueOnce(
          new Error('Request failed with status code 404'),
        );
      const store = createRootStore();
      //@ts-ignore
      store.dispatch(fetchEmployeeDetails('John Doe'));

      await waitFor(() =>
        expect(store.getState().app.message).toEqual(
          'Request failed with status code 404',
        ),
      );
      expect(store.getState().employee.collection).toEqual({});
    });
  });
});
