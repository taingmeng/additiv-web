import { setErrorMessage } from './../app/actions';
import { Dispatch } from 'redux';
import { RootState } from '../store';

import * as api from './api';
import { getCollection } from './selectors';
import {
  EmployeesResponse,
  SetLoading,
  SetEmployees,
  SetEmployeeDetails,
  EmployeeAction,
  Employee,
} from './types';
import { AppAction } from '../app/types';

export const SET_LOADING = 'Additiv/employee/SET_LOADING';
export const SET_EMPLOYEES = 'Additiv/employee/SET_EMPLOYEES';
export const SET_EMPLOYEE_DETAILS = 'Additiv/employee/SET_EMPLOYEE_DETAILS';

export const setLoading = (loading: boolean): SetLoading => ({
  type: SET_LOADING,
  payload: loading,
});

export const setEmployees = (response: EmployeesResponse): SetEmployees => ({
  type: SET_EMPLOYEES,
  payload: response,
});

export const setEmployeeDetails = (response: Employee): SetEmployeeDetails => ({
  type: SET_EMPLOYEE_DETAILS,
  payload: response,
});

export const fetchEmployees = () => async (
  dispatch: Dispatch<EmployeeAction | AppAction>,
): Promise<void> => {
  try {
    const response = await api.listAllEmployeesApi();
    dispatch(setEmployees(response));
  } catch (error) {
    dispatch(setErrorMessage(error.message));
  }
};

export const fetchEmployeeDetails = (name: string) => async (
  dispatch: Dispatch<EmployeeAction | AppAction>,
  getState: () => RootState,
): Promise<void> => {
  try {
    const state = getState();
    const collection = getCollection(state);
    dispatch(setLoading(true));
    const [position, info] = await api.getEmployeeDetailsApi(name);
    const directSubordinates = info ? info['direct-subordinates'] : [];
    const indirectSubs = new Set<string>();
    const queue = [...directSubordinates];
    while (queue.length) {
      const subName = queue.shift();

      // Prevent cycle
      if (!indirectSubs.has(subName)) {
        indirectSubs.add(subName);
        if (collection[subName]) {
          for (const nextSub of collection[subName].directSubordinates) {
            queue.push(nextSub);
          }
        } else {
          const [subPosition, subInfo] = await api.getEmployeeDetailsApi(
            subName,
          );
          const nextSubs = subInfo ? subInfo['direct-subordinates'] : [];
          const subEmployee: Employee = {
            name: subName,
            position: subPosition,
            directSubordinates: nextSubs,
            indirectSubordinates: [],
          };
          dispatch(setEmployeeDetails(subEmployee));
          for (const nextSub of nextSubs) {
            queue.push(nextSub);
          }
        }
      }
    }
    directSubordinates.forEach((name) => indirectSubs.delete(name));
    indirectSubs.delete(name);

    const employee: Employee = {
      name,
      position,
      directSubordinates,
      indirectSubordinates: Array.from(indirectSubs),
    };
    dispatch(setEmployeeDetails(employee));
  } catch (error) {
    dispatch(setErrorMessage(error.message));
  } finally {
    dispatch(setLoading(false));
  }
};
