import axios from 'axios';
import { EmployeesResponse, EmployeeDetailsResponse } from './types';

const BASE_URL = 'https://api.additivasia.io/api/v1';

export const listAllEmployeesApi = async (): Promise<EmployeesResponse> => {
  const res = await axios.get(`${BASE_URL}/assignment/employees`);
  return res.data;
};

export const getEmployeeDetailsApi = async (
  name: string,
): Promise<EmployeeDetailsResponse> => {
  const encodedName = encodeURIComponent(name);
  const res = await axios.get(
    `${BASE_URL}/assignment/employees/${encodedName}`,
  );
  return res.data;
};
