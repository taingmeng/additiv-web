import { SET_EMPLOYEES, SET_EMPLOYEE_DETAILS, SET_LOADING } from './actions';
import { EmployeeAction, EmployeeCollection, EmployeesResponse } from './types';

interface EmployeeState {
  loading: boolean;
  employees: EmployeesResponse;
  collection: EmployeeCollection;
}

export const initialState: EmployeeState = {
  loading: false,
  collection: {},
  employees: [],
};

const EmployeeReducer = (
  state: EmployeeState = initialState,
  action: EmployeeAction,
): EmployeeState => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case SET_EMPLOYEES:
      return {
        ...state,
        employees: action.payload,
      };
    case SET_EMPLOYEE_DETAILS:
      return {
        ...state,
        collection: {
          ...state.collection,
          [action.payload.name]: action.payload,
        },
      };

    default:
      return state;
  }
};

export default EmployeeReducer;
