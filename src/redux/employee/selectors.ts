import { EmployeeCollection, EmployeesResponse } from './types';
import { RootState } from '../store';

export const getLoading = (state: RootState): boolean => state.employee.loading;
export const getEmployees = (state: RootState): EmployeesResponse =>
  state.employee.employees;
export const getCollection = (state: RootState): EmployeeCollection =>
  state.employee.collection;
