import { SET_EMPLOYEES, SET_EMPLOYEE_DETAILS, SET_LOADING } from './actions';
export type EmployeesResponse = string[];

export type position = string;

export type DirectSubordinates = {
  'direct-subordinates': string[];
};

export type EmployeeDetailsResponse = [position, DirectSubordinates];

export type Employee = {
  name: string;
  position: string;
  directSubordinates: string[];
  indirectSubordinates: string[];
};

export type EmployeeCollection = {
  [key: string]: Employee;
};

export interface SetLoading {
  type: typeof SET_LOADING;
  payload: boolean;
}
export interface SetEmployees {
  type: typeof SET_EMPLOYEES;
  payload: EmployeesResponse;
}

export interface SetEmployeeDetails {
  type: typeof SET_EMPLOYEE_DETAILS;
  payload: Employee;
}

export type EmployeeAction = SetLoading | SetEmployees | SetEmployeeDetails;
