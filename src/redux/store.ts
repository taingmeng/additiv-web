import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import AppReducer from './app/reducer';
import EmployeeReducer from './employee/reducer';

const reducers = {
  app: AppReducer,
  employee: EmployeeReducer,
};

const rootReducer = combineReducers(reducers);

export type RootState = ReturnType<typeof rootReducer>;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const createRootStore = () =>
  createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

const store = createRootStore();

export default store;
